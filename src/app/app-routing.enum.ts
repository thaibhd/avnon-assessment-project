export enum MainRoutingEnum {
  DEFAULT = "form",
  BUILDER = "form/builder",
  ANSWERS = "form/answers",
  NOT_FOUND = "not-found"
}
