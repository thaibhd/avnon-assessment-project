import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuilderComponent } from './pages/builder/builder.component';
import { AnswersComponent } from './pages/answers/answers.component';
import { MainRoutingEnum } from './app-routing.enum';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  { path: '', redirectTo: MainRoutingEnum.BUILDER, pathMatch: 'full' },
  { path: MainRoutingEnum.DEFAULT, redirectTo: MainRoutingEnum.BUILDER, pathMatch: 'full' },
  { path: MainRoutingEnum.BUILDER, component: BuilderComponent },
  { path: MainRoutingEnum.ANSWERS, component: AnswersComponent },
  { path: MainRoutingEnum.NOT_FOUND, component: NotFoundComponent },
  { path: '**', redirectTo: MainRoutingEnum.NOT_FOUND}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
