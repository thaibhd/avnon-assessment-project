import { Injectable } from '@angular/core';
import { QuestionModel } from '../models/form.model';
import { QuestionTypeEnum } from '../enums/form.enum';
import { AddQuestionModel } from '../models/add-question.model';

@Injectable({
  providedIn: 'root'
})
export class AddQuestionService {
  defaultQuestionData: AddQuestionModel = {
    type: {
      title: "Question type",
      isRequired: true,
      data: [{
        key: "Paragraph",
        value: QuestionTypeEnum.PARAGRAPH
      },{
        key: "Checkbox",
        value: QuestionTypeEnum.CHECKBOX
      }],
      value: "",
    },
    question: {
      title: "Question",
      placeholder: "Type question here",
      value: "",
      isRequired: true
    },
    checkboxOptions: [],
    allowOwnAnswer: {
      title: "Allow user to specify their own answer",
      value: false,
      id: "allowOwnAnswer"
    },
    isRequired: {
      title: "This field is required",
      value: false,
      id: "isRequired"
    }
  } 

  constructor() { }



}
