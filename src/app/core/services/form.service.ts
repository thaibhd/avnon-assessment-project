import { Injectable } from '@angular/core';
import { QuestionModel } from '../models/form.model';
import { QuestionTypeEnum } from '../enums/form.enum';
import { AddQuestionModel } from '../models/add-question.model';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  currentFormData: QuestionModel[] = [];
  sampleFormData: QuestionModel[] = [
    {
      title: "Please tell us about yourself",
      type: QuestionTypeEnum.PARAGRAPH,
      answer: "",
      isRequired: true,
      checkbox: [],
      isAllowOwnAnswer: false
    },{
      title: "Please select the languages you know",
      type: QuestionTypeEnum.CHECKBOX,
      answer: "",
      isRequired: true,
      isAllowOwnAnswer: false,
      checkbox: [
        {
          title: "TypeScript",
          value: "typescript",
          isCheck: false
        },{
          title: "Python",
          value: "python",
          isCheck: false
        },{
          title: "C#",
          value: "cshape",
          isCheck: false
        },{
          title: "Other",
          value: "other",
          isCheck: false
        }
      ]
    }
  ]
  constructor() { }

  getSampleFormData(){
    return this.sampleFormData;
  }

  createNewQuestion(data: AddQuestionModel) {
    let _data = {
      title: data.question.value ? data.question.value : "",
      type: data.type.value,
      answer: "",
      isRequired: data.isRequired.value,
      checkbox: [],
      isAllowOwnAnswer: data.allowOwnAnswer.value
    } as QuestionModel;
    for (let answer of data.checkboxOptions) {
      _data.checkbox.push({
        title: answer,
        value: answer,
        isCheck: false
      })
    }
    return _data;
  }

  setCurrentFormData(data: QuestionModel[]){
    this.currentFormData = data;
  }

  getCurrentFormData(){
    return(this.currentFormData);
  }

}
