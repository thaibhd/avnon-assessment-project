import { QuestionTypeEnum } from "../enums/form.enum";

export class FormBuilderModel {
  constructor() {
  }
  title: string;
  questions: QuestionModel[]
}

export class QuestionModel {
  constructor() {
  }
  title: string;
  type: QuestionTypeEnum;
  answer: string;
  checkbox: CheckboxModel[];
  isRequired: boolean;
  isAllowOwnAnswer?: boolean;
}

export class CheckboxModel {
  constructor() {
  }
  title: string;
  value: string;
  isCheck: boolean;
}