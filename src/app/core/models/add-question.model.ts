import { QuestionTypeEnum } from "../enums/form.enum";

export class AddQuestionModel {
  constructor() {
  }
  type: AddQuestionTypeModel;
  question: AddQuestionQuestionModel;
  checkboxOptions: string[];
  allowOwnAnswer: AddQuestionCheckboxModel;
  isRequired: AddQuestionCheckboxModel;
}

export class AddQuestionTypeModel {
  constructor() {
  }
  title: string;
  value: QuestionTypeEnum | string;
  data: SelectQuestionTypeModel[];
  isRequired: boolean;
}

export class SelectQuestionTypeModel {
  constructor() {
  }
  key: string;
  value: string;
}

export class AddQuestionQuestionModel {
  constructor() {
  }
  title: string;
  value: string;
  placeholder?: string;
  isRequired: boolean;
}

export class AddQuestionCheckboxModel {
  constructor() {
  }
  title: string;
  value: boolean;
  id: string;
}


