import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { QuestionTypeEnum } from 'src/app/core/enums/form.enum';
import { AddQuestionModel } from 'src/app/core/models/add-question.model';
import { QuestionModel } from 'src/app/core/models/form.model';
import { AddQuestionService } from 'src/app/core/services/add-question.service';

@Component({
  selector: 'add-question-modal',
  templateUrl: './add-question-modal.component.html',
  styleUrls: ['./add-question-modal.component.scss']
})
export class AddQuestionModalComponent implements OnInit {
  @ViewChild('addQuestionModal') modal: any;
  closeResult: string;
  modalReference: NgbModalRef;
  size: string = "md";
  addQuestionData: AddQuestionModel = new AddQuestionModel;
  questionType = QuestionTypeEnum;
  answerList: any[] = [{value: ""}];

  @Output() onAddedSuccessful = new EventEmitter<object>();
  constructor( private modalService: NgbModal, private addQuestionService: AddQuestionService) { }

  ngOnInit(): void {
  }

  submit(){
    for (let answer of this.answerList) if (answer.value) this.addQuestionData.checkboxOptions.push(answer.value);
    this.onAddedSuccessful.emit(this.addQuestionData);
    this.close();
  }

  open() {
    this.modalReference = this.modalService.open(this.modal, {
      ariaLabelledBy: 'modal-basic-title', 
      centered: false,
      size: this.size
    });
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.addQuestionData = this.addQuestionService.defaultQuestionData;
    this.answerList = [{value: ""}];
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  close(){
    this.modalReference.close();
  }

  onSelectQuestionType($event) {
    console.log($event)
  }

  addAnswers(){
    this.answerList.push({value: ""});
  }

  onAnswerFill(index:number, $event:string){
    this.answerList[index] = $event;
  }

}
