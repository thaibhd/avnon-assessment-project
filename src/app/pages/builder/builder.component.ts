import { Component, OnInit, ViewChild } from '@angular/core';
import { QuestionTypeEnum } from 'src/app/core/enums/form.enum';
import { QuestionModel } from 'src/app/core/models/form.model';
import { FormService } from 'src/app/core/services/form.service';
import { AddQuestionModalComponent } from '../add-question-modal/add-question-modal.component';
import { AddQuestionModel } from 'src/app/core/models/add-question.model';
import { MainRoutingEnum } from 'src/app/app-routing.enum';
import { Router } from '@angular/router';

@Component({
  selector: 'app-builder',
  templateUrl: './builder.component.html',
  styleUrls: ['./builder.component.scss']
})
export class BuilderComponent implements OnInit {
  formData: QuestionModel[] = [];
  questionType = QuestionTypeEnum;
  sampleFormData: QuestionModel[] = [];

  @ViewChild(AddQuestionModalComponent) _addQuestionModal: AddQuestionModalComponent;

  constructor(private formService: FormService, private routerService: Router) { }

  ngOnInit(): void {
    this.formData = this.formService.getCurrentFormData();
  }
  //#region Add Question
  addQuestion(){
    this._addQuestionModal.open();
  }

  reloadFormBuilder(data: AddQuestionModel){
    let newQuestion = this.formService.createNewQuestion(data);
    this.formData.push(newQuestion);
    this.formService.setCurrentFormData(this.formData);
  }
  //#endregion

  //#region View Answer
  reviewAnswers(){
    this.routerService.navigate([MainRoutingEnum.ANSWERS], {queryParams: {}});
  }
  //#endregion
}
