import { Component, OnInit, ViewChild } from '@angular/core';
import { QuestionTypeEnum } from 'src/app/core/enums/form.enum';
import { QuestionModel } from 'src/app/core/models/form.model';
import { FormService } from 'src/app/core/services/form.service';
import { AddQuestionModalComponent } from '../add-question-modal/add-question-modal.component';
import { AddQuestionModel } from 'src/app/core/models/add-question.model';
import { Router } from '@angular/router';
import { MainRoutingEnum } from 'src/app/app-routing.enum';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.scss']
})
export class AnswersComponent implements OnInit {
  formData: QuestionModel[] = [];
  questionType = QuestionTypeEnum;
  constructor(private formService: FormService, private routerService: Router) { }

  ngOnInit(): void {
    this.formData = this.formService.getCurrentFormData();
    console.log(this.formData)
  }

  back(){
    this.routerService.navigate([MainRoutingEnum.BUILDER], {queryParams: {}});

  }

}
