import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainRoutingEnum } from 'src/app/app-routing.enum';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor(private routerService: Router) { }

  ngOnInit(): void {
  }

  back(){
    this.routerService.navigate([MainRoutingEnum.BUILDER], {queryParams: {}});
  }
}
